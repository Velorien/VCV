﻿using System;
using System.Collections.Generic;
using System.Linq;
using VCV.Web.Models;

namespace VCV.Web.Services
{
    public class ApplicationService
    {
        public AppDefaultsProvider defaultsProvider;

        public ApplicationService(AppDefaultsProvider defaultsProvider) =>
            this.defaultsProvider = defaultsProvider;

        public List<WindowState> Applications { get; } = new List<WindowState>();

        public void Close(Guid id) => Applications.Remove(Applications.Single(x => x.Id == id));

        public void Launch(Type applicationType, FileSystemEntry file = null)
        {
            var appDefaults = defaultsProvider.GetForType(applicationType);
            if (appDefaults == null) return;
            var state = WindowState.Create(defaultsProvider.GetForType(applicationType), file);
            Applications.Add(state);
            ApplicationLaunched?.Invoke();
            Activate(state.Id, false);
        }

        public void Activate(Guid id, bool restore)
        {
            foreach (var item in Applications)
            {
                item.IsActive = item.Id == id;
                if (restore && item.IsActive) item.IsMinimized = false;
            }

            ApplicationActivated?.Invoke();
        }

        public Action ApplicationLaunched { get; set; }

        public Action ApplicationActivated { get; set; }
    }
}
