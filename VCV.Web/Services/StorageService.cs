﻿using Blazor.Extensions.Storage;
using Microsoft.AspNetCore.Blazor.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VCV.Web.Apps;
using VCV.Web.Models;

namespace VCV.Web.Services
{
    public class StorageService
    {
        private const string FileSystem = "filesystem";
        private readonly LocalStorage storage;
        private Task initialization;

        private readonly Dictionary<string, (string icon, Type launch)> extensionRegistry
            = new Dictionary<string, (string icon, Type launch)>
            {
                { "md", ("⇓", typeof(MarkdownEditor)) }
            };

        public FileSystemEntry Root { get; private set; }

        public StorageService(LocalStorage storage)
        {
            this.storage = storage;
            Initialize();
        }

        public Task Initialize()
        {
            if (initialization != null) return initialization;
            else initialization = Task.Run(async () =>
            {
                Root = await storage.GetItem<FileSystemEntry>(FileSystem);
                if (Root == null)
                {
                    Root = new FileSystemEntry { Name = "root", IsDirectory = true };
                    await storage.SetItem(FileSystem, Root);
                }
            });

            return initialization;
        }

        public async Task CreateDirectory(FileSystemEntry parent, string name)
        {
            parent.ThrowIfNameExists(name);
            parent.AddChild(new FileSystemEntry { Name = name, IsDirectory = true });
            await storage.SetItem(FileSystem, Root);
        }

        public async Task<FileSystemEntry> CreateFile(FileSystemEntry parent, string name, string content = "")
        {
            parent.ThrowIfNameExists(name);
            string id = Guid.NewGuid().ToString();
            var file = new FileSystemEntry { Name = name, Id = id };
            parent.AddChild(file);
            file.Icon = GetIconForExtension(file.Extension);
            await storage.SetItem(FileSystem, Root);
            await SetFileContent(file, content);

            return file;
        }

        public async Task Rename(FileSystemEntry entry, string newName, FileSystemEntry parent)
        {
            if (entry.Name == newName) return;
            parent.ThrowIfNameExists(newName);
            entry.Name = newName;
            entry.LastModification = DateTime.Now;
            if(!entry.IsDirectory) entry.Icon = GetIconForExtension(entry.Extension);
            await storage.SetItem(FileSystem, Root);
        }

        public Task<string> GetFileContent(FileSystemEntry file) => storage.GetItem<string>(file.Id);

        public async Task DeleteFile(FileSystemEntry file, FileSystemEntry parent)
        {
            parent.RemoveChild(file);
            await storage.RemoveItem(file.Id);
        }

        public async Task DeleteDirectory(FileSystemEntry directory, FileSystemEntry parent, bool isTopLevel = true)
        {
            var filesToDelete = new List<FileSystemEntry>();
            foreach (var item in directory.Children)
            {
                if (item.IsDirectory) await DeleteDirectory(item, directory, false);
                else filesToDelete.Add(item);
            }

            foreach (var item in filesToDelete) await DeleteFile(item, directory);

            if (isTopLevel) parent.RemoveChild(directory);
            await storage.SetItem(FileSystem, Root);
        }

        public async Task SetFileContent(FileSystemEntry file, string content)
        {
            await storage.SetItem(file.Id, content);
            file.LastModification = DateTime.Now;
            await storage.SetItem(FileSystem, Root);
        }

        public async Task Copy(FileSystemEntry item, FileSystemEntry from, FileSystemEntry to, string newName = null, bool isTopLevel = true)
        {
            to.ThrowIfNameExists(newName ?? item.Name);
            if (item.IsDirectory)
            {
                var newDirectory = new FileSystemEntry { Name = newName ?? item.Name, IsDirectory = true };
                to.AddChild(newDirectory);
                foreach (var fse in item.Children) await Copy(fse, item, newDirectory, isTopLevel: false);
            }
            else
            {
                var newFile = new FileSystemEntry { Name = newName ?? item.Name, Id = Guid.NewGuid().ToString() };
                await storage.SetItem(newFile.Id, await storage.GetItem<string>(item.Id));
                to.AddChild(newFile);
            }

            if (isTopLevel) await storage.SetItem(FileSystem, Root);
        }

        public async Task Move(FileSystemEntry item, FileSystemEntry from, FileSystemEntry to, string newName = null)
        {
            to.ThrowIfNameExists(newName ?? item.Name);
            from.RemoveChild(item);
            to.AddChild(item);
            await storage.SetItem(FileSystem, Root);
        }

        public async Task Clear()
        {
            await storage.Clear();
            Root = null;
            initialization = null;
            await Initialize();
        }

        public string GetIconForExtension(string extension) =>
            extensionRegistry.ContainsKey(extension) ? extensionRegistry[extension].icon : "🗋";

        public Type GetLaunchTypeForExtension(string extension) =>
            extensionRegistry.ContainsKey(extension) ? extensionRegistry[extension].launch : null;
    }
}
