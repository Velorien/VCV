﻿using Microsoft.AspNetCore.Blazor.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using VCV.Web.Apps;
using VCV.Web.Models;
using VCV.Web.Services;

namespace VCV.Web.Components
{
    public class StartMenuComponent : BlazorComponent
    {
        protected List<MenuItem> menuItems;

        protected List<MenuItem> currentTree = new List<MenuItem>();

        [Inject]
        protected AppDefaultsProvider DefaultsProvider { get; set; }

        [Inject]
        protected ApplicationService ApplicationService { get; set; }

        protected override void OnInit()
        {
            menuItems = new List<MenuItem> {
                new MenuItem(DefaultsProvider.GetForType(typeof(CurriculumVitae))),
                new MenuItem("Apps", "⌨", new List<MenuItem> {
                    new MenuItem(DefaultsProvider.GetForType(typeof(Zalgo))),
                    new MenuItem(DefaultsProvider.GetForType(typeof(Weather))),
                    new MenuItem(DefaultsProvider.GetForType(typeof(MarkdownEditor))),
                    new MenuItem(DefaultsProvider.GetForType(typeof(Dice)))
                }),
                new MenuItem("Games", "♞", new List<MenuItem> {
                    new MenuItem(DefaultsProvider.GetForType(typeof(Landmines)))
                }),
                new MenuItem(DefaultsProvider.GetForType(typeof(StorageExplorer))),
                new MenuItem(DefaultsProvider.GetForType(typeof(About)))
            };
        }

        protected void OnMenuHover(MenuItem item)
        {
            if (item.HasChildren)
            {
                currentTree.Clear();
                currentTree.AddRange(item.BuildTree());
            }
            else if (item.Parent == null || item.Parent.Children.Contains(item))
            {
                currentTree.Clear();
                var tree = item.BuildTree();
                currentTree.AddRange(tree.Take(tree.Count() - 1));
            }
        }

        protected string GetOffsets(MenuItem item)
        {
            int left = 300 + 200 * currentTree.IndexOf(item);
            int top = 0;
            var current = item;
            while (current.Parent != null)
            {
                top += 30 * current.Parent.Children.IndexOf(current);
                current = current.Parent;
            }

            top += 40 * menuItems.IndexOf(currentTree.First());

            string offsets = $"left: {left}px; top: {top}px";
            return offsets;
        }

        protected void MenuItemClick(MenuItem item) => ApplicationService.Launch(item.Launch);
    }
}
