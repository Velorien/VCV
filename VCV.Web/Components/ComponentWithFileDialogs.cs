﻿using Microsoft.AspNetCore.Blazor.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VCV.Web.Models;
using VCV.Web.Services;

namespace VCV.Web.Components
{
    public abstract class ComponentWithFileDialogs : BlazorComponent
    {
        protected FileSystemEntry currentFile;
        protected bool showOpenDialog;
        protected bool showSaveDialog;

        [Inject]
        private StorageService Storage { get; set; }

        protected void CloseFileDialog()
        {
            showOpenDialog = showSaveDialog = false;
            StateHasChanged();
        }

        protected Task<string> GetFileContent(FileSystemEntry file) => Storage.GetFileContent(file);

        protected Task SetFileContent(FileSystemEntry file, string content) => Storage.SetFileContent(file, content);

        protected void OnFileSaved(FileSystemEntry file)
        {
            currentFile = file;
            CloseFileDialog();
            StateHasChanged();
        }

        protected async Task SaveFile(string content)
        {
            if (currentFile == null)
            {
                showSaveDialog = true;
                return;
            }

            await SetFileContent(currentFile, content);
        }

        protected void OnFileOpened(FileSystemEntry file, Action processContent)
        {
            currentFile = file;
            processContent?.Invoke();
            CloseFileDialog();
            StateHasChanged();
        }
    }
}
