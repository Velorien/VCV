﻿using Microsoft.AspNetCore.Blazor.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VCV.Web.Models;
using VCV.Web.Services;

namespace VCV.Web.Components
{
    public class TaskBarComponent : BlazorComponent
    {
        protected string time = DateTime.Now.ToString("HH:mm");
        private Timer timer;
        private bool showColon;

        [Parameter]
        protected Action StartButtonPressed { get; set; }

        [Parameter]
        protected Action<Guid> AppClicked { get; set; }

        [Parameter]
        protected IEnumerable<WindowState> Windows { get; set; }

        [Inject]
        public ApplicationService ApplicationService { get; set; }

        protected override void OnInit()
        {
            timer = new Timer(_ => { time = DateTime.Now.ToString(showColon ? "HH:mm" : "HH mm"); showColon = !showColon; StateHasChanged(); }, null, 0, 500);
        }
    }
}
