﻿using System.Collections.Generic;

namespace VCV.Web.Models
{
    public class Skill
    {
        public string Name { get; set; }

        public int Level { get; set; }

        public string Description { get; set; }
    }

    public class SkillGroup
    {
        public string Name { get; set; }

        public IEnumerable<Skill> Skills { get; set; }
    }
}
