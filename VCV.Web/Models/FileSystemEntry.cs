﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace VCV.Web.Models
{
    public class FileSystemEntry : IComparable<FileSystemEntry>
    {
        public string Name { get; set; }

        public string Id { get; set; }

        public DateTime LastModification { get; set; } = DateTime.Now;

        public List<FileSystemEntry> UnsortedChildren { get; set; }
            = new List<FileSystemEntry>();

        [IgnoreDataMember]
        public SortedSet<FileSystemEntry> Children
        {
            get
            {
                if (children == null) children = new SortedSet<FileSystemEntry>(UnsortedChildren);
                return children;
            }
        }
        private SortedSet<FileSystemEntry> children;

        public bool IsDirectory { get; set; }

        public string Icon { get; set; }

        public string Extension => Name.Split('.').Last();

        public int CompareTo(FileSystemEntry other)
        {
            if (IsDirectory && !other.IsDirectory) return -1;
            else if (!IsDirectory && other.IsDirectory) return 1;
            else return Name.CompareTo(other.Name);
        }

        public void ThrowIfNameExists(string name)
        {
            if (Children.Any(x => x.Name == name)) throw new Exception();
        }

        public void AddChild(FileSystemEntry entry)
        {
            Children.Add(entry);
            UnsortedChildren.Add(entry);
        }

        public void RemoveChild(FileSystemEntry entry)
        {
            Children.Remove(entry);
            UnsortedChildren.Remove(entry);
        }
    }
}
