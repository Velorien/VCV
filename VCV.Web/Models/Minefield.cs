﻿namespace VCV.Web.Models
{
    public class Minefield
    {
        public bool HasMine { get; set; }

        public int AdjacentMines { get; set; }

        public bool Flagged { get; set; }

        public bool Revealed { get; set; }
    }
}
