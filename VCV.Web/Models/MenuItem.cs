﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VCV.Web.Models
{
    public class MenuItem
    {
        public MenuItem(AppDefaults appDefaults) : this(appDefaults.Title, appDefaults.Icon) => Launch = appDefaults.Type;

        public MenuItem(string name, string icon, List<MenuItem> children = null)
        {
            Title = name;
            Icon = icon;
            Children = children ?? new List<MenuItem>();
            foreach (var item in Children) item.Parent = this;
        }

        public string Title { get; }

        public string Icon { get; }

        public Type Launch { get; }

        public MenuItem Parent { get; set; }

        public List<MenuItem> Children { get; }

        public bool HasChildren => Children.Any();

        public IEnumerable<MenuItem> BuildTree()
        {
            var tree = new List<MenuItem>();
            var current = this;
            while(current != null)
            {
                tree.Add(current);
                current = current.Parent;
            }

            tree.Reverse();
            return tree;
        }
    }
}
