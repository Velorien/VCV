﻿using System;
using System.Collections.Generic;

namespace VCV.Web.Models
{
    public class Employment
    {
        public string CompanyName { get; set; }

        public string Position { get; set; }

        public DateTime From { get; set; }

        public DateTime? To { get; set; }

        public string Description { get; set; }

        public IEnumerable<string> Keywords { get; set; }
    }
}
