﻿using System;

namespace VCV.Web.Models
{
    public class WindowState
    {
        public WindowState() => Id = Guid.NewGuid();

        public static WindowState Create(AppDefaults defaults, FileSystemEntry file = null) =>
            new WindowState
            {
                CanMaximize = defaults.CanMaximize,
                CanResize = defaults.CanResize,
                HasHelp = defaults.HasHelp,
                Height = defaults.Height,
                Width = defaults.Width,
                Icon = defaults.Icon,
                Type = defaults.Type,
                Title = defaults.Title,
                LaunchingFile = file,
                HelpText = defaults.HelpText
            };

        public Guid Id { get; }

        public Type Type { get; set; }

        public bool CanMaximize { get; set; }

        public bool CanResize { get; set; }

        public bool HasHelp { get; set; }

        public string Title { get; set; }

        public string Icon { get; set; }

        public bool IsActive { get; set; }

        public bool IsMinimized { get; set; }

        public bool IsMaxixmized { get; set; }

        public long PosX { get; set; } = 1;

        public long PosY { get; set; }

        public long Height { get; set; }

        public long Width { get; set; }

        public string HelpText { get; set; }

        public FileSystemEntry LaunchingFile { get; set; }

        public string Style =>
            IsMinimized ? "display: none;" :
            (IsMaxixmized ? "top: 0px; left: 1px; bottom: 0px; right: 1px; " :
                $"top: {PosY}px; left: {PosX}px; height: {Height}px; width: {Width}px; ")
            + (IsActive ? "z-index: 99; " : string.Empty);
    }
}
