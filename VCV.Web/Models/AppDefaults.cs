﻿using System;

namespace VCV.Web.Models
{
    public class AppDefaults
    {
        public AppDefaults(
            Type type,
            string icon,
            string title = null,
            long height = 600,
            long width = 800,
            bool canMaximize = true,
            bool canResize = true,
            string helpText = null)
        {
            Type = type;
            Icon = icon;
            Title = title ?? type.Name;
            Height = height;
            Width = width;
            CanMaximize = canMaximize;
            CanResize = canResize;
            HelpText= helpText;
        }

        public string Title { get; }

        public string Icon { get; }

        public long Height { get; }

        public long Width { get; }

        public bool CanMaximize { get; }

        public bool CanResize { get; }

        public bool HasHelp => HelpText != null;

        public string HelpText { get; }

        public Type Type { get; } 
    }
}
